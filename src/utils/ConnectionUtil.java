/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 *
 * @author codeOlam
 */
public class ConnectionUtil {
    Connection conn = null;
    
    
    public static Connection conDB() {
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); 
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_test_db?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "your_db_name", "your_db_passowrd");
            return con;
                    } 
        catch (Exception ex) {
            return null;
        }
    }
}
