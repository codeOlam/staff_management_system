/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdash.controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import utils.ConnectionUtil;

/**
 *
 * @author codeOlam
 */
public class FXMLDocumentController implements Initializable {
    

    
    //New function for the app
    @FXML
    private Button btnStudents;

    @FXML
    private Button btnTeachers;

    @FXML
    private Button btnaddNewStd;

    @FXML
    private Button btnUsers;

    @FXML
    private Button btnSettings;

    @FXML
    private Pane pnlStudents;

    @FXML
    private Label lblStatus_std;

    @FXML
    private Label lblStaus_stdMin;
    
    @FXML
    private FontAwesomeIconView btnclose;
    
    @FXML
    private GridPane pnStudents;

    @FXML
    private GridPane pnTeachers;

    @FXML
    private GridPane pnFees;

    @FXML
    private GridPane pnUsers;

    @FXML
    private GridPane pnSettings;
    
    // For Login Controls
    @FXML
    private PasswordField pswd_field;
    @FXML
    private TextField uname_textfield;
    @FXML
    private Button btn_login;

    @FXML
    private Button btnClose;
    @FXML
    private Label lbl_Errors;
    
    
    
    // Add new Student 
    @FXML
    private TextField tfield_fname;

    @FXML
    private TextField tfield_lname;

    @FXML
    private Button btnSave;

    @FXML
    private DatePicker datepicker;
    

    @FXML
    private TableView<user> fx_tableview;
    @FXML
    private TableColumn<user, String> col_id;
    @FXML
    private TableColumn<user, String> col_fname;
    @FXML
    private TableColumn<user, String> col_lname;
    @FXML
    private TableColumn<user, String> col_createdAt;
    
    //Table Initializations
    private ObservableList<user>data;
    private ConnectionUtil connDB;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        connDB = new ConnectionUtil();
    }

    // clearing text filed
    public void clearField(){
        tfield_fname.clear();
        tfield_lname.clear();
        datepicker.getEditor().clear();
    }
    
    @FXML
    public void handleClicks(ActionEvent event){
        
        if(event.getSource() == btnStudents){
            lblStaus_stdMin.setText("/home/staff");
            lblStatus_std.setText("Staff");
            pnlStudents.setBackground(new Background(new BackgroundFill(Color.rgb(113, 86, 221), CornerRadii.EMPTY, Insets.EMPTY)));

            try {
                Connection conn = connDB.conDB();
                data = FXCollections.observableArrayList();
                
                //Execute query and store result in a resultset
                ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM users");
                
                
                while(rs.next()){
                    data.add(new user(rs.getInt(1), 
                                        rs.getString(2), 
                                        rs.getString(3), 
                                        rs.getString(4)));
                }
                
                
            } 
            catch (SQLException ex) {
                System.err.println("Error"+ ex);
            }
            
            col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
            col_fname.setCellValueFactory(new PropertyValueFactory<>("fname"));
            col_lname.setCellValueFactory(new PropertyValueFactory<>("lname"));
            col_createdAt.setCellValueFactory(new PropertyValueFactory<>("createdAt"));
            
            fx_tableview.setItems(null);
            fx_tableview.setItems(data);
            
            pnStudents.toFront();
        }
        else{
            
        }
        if(event.getSource()==btnTeachers){
            lblStaus_stdMin.setText("/home/task_record");
            lblStatus_std.setText("Task Record");
            pnlStudents.setBackground(new Background(new BackgroundFill(Color.rgb(43, 63, 99), CornerRadii.EMPTY, Insets.EMPTY)));
            pnTeachers.toFront();
            
        }
        else{
            
        }
        if(event.getSource()==btnaddNewStd){
            lblStaus_stdMin.setText("/home/add_new_staff");
            lblStatus_std.setText("Add New Staff");
            pnlStudents.setBackground(new Background(new BackgroundFill(Color.rgb(43, 99, 63), CornerRadii.EMPTY, Insets.EMPTY)));
            pnFees.toFront();
        }
        else{
            
        }
        if(event.getSource()==btnUsers){
            lblStaus_stdMin.setText("/home/payroll");
            lblStatus_std.setText("Payroll");
            pnlStudents.setBackground(new Background(new BackgroundFill(Color.rgb(99, 43, 63), CornerRadii.EMPTY, Insets.EMPTY)));
            pnUsers.toFront();
        }
        else{
            
        }
        if(event.getSource()==btnSettings){
            lblStaus_stdMin.setText("/home/settings");
            lblStatus_std.setText("Settings");
            pnlStudents.setBackground(new Background(new BackgroundFill(Color.rgb(42, 28, 66), CornerRadii.EMPTY, Insets.EMPTY)));
            pnSettings.toFront();
        }
        else{
            
        }
        
        if(event.getSource()==btnSave){
            try {
                String fname = tfield_fname.getText();
                String lname = tfield_lname.getText();
                LocalDate createdAt = datepicker.valueProperty().get();
                
                
                Connection conn = connDB.conDB();
                
                String sql = "INSERT INTO users(fname, lname, createdAt) VALUES(?, ?, ?)";
                PreparedStatement ps;
                ps = conn.prepareStatement(sql);
                
                ps.setString(1, fname);
                ps.setString(2, lname);
                ps.setDate(3, Date.valueOf(createdAt));
                
                ps.executeUpdate();
                clearField();
                showSuccessMsg();
                
            } 
            catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        else{
            
        }

    }
    
    
    @FXML
    public void BtnClose(MouseEvent event){
        if (event.getSource() == btnclose){
            System.exit(0);
        }
    }

    @FXML
    public void handleClose(MouseEvent event) {
        if (event.getSource() == btnClose){
            System.exit(0);
        }
    }
    
    

 
    // For Login
    Connection conn = connDB.conDB();
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    
    private String logIn(){
        String email = uname_textfield.getText();
        String password = pswd_field.getText();
        
        //query
        String sql = "SELECT * FROM admins Where email = ? and password = ?";
        
        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()){
                lbl_Errors.setTextFill(Color.TOMATO);
                lbl_Errors.setText("Fields cannot be Empty or Wrong Credentials were Entered!");
                return "Error!";
            }
            else{
                lbl_Errors.setTextFill(Color.GREEN);
                lbl_Errors.setText("Login Succesfull! Redirecting...");
                return "Success!";
            }
        } 
        catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            return "Exception";
        }
    }
    
    
    private void showSuccessMsg(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("New Student Successfully Added to data base");
        alert.setHeaderText(null);
        alert.showAndWait();
    }
   
    
    
    @FXML
    public void handleButtonAction(MouseEvent event) {
        if (event.getSource() == btn_login){
           //Login code here
            //logIn();
            if(logIn().equals("Success!")){
                try {
                    Node node = (Node) event.getSource();
                    Stage stage = (Stage)node.getScene().getWindow();
                    stage.close();
                    
                    Scene scene = new Scene(FXMLLoader.load(getClass().getClassLoader().getResource("sdash/fxml/Main.fxml")));
                    stage.setScene(scene);
                    stage.show();
                } 
                catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
           }
        }
        
    }
    
    
        
    

}
