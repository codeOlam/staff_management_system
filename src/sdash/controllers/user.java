/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdash.controllers;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author codeOlam
 */
public class user {
    private final IntegerProperty id;
    private final StringProperty fname;
    private final StringProperty lname;
    private final StringProperty createdAt;
    
    
    public user(int u_id, String u_fname, String u_lname, String u_createdAt){
        this.id = new SimpleIntegerProperty(u_id);
        this.fname = new SimpleStringProperty(u_fname);
        this.lname = new SimpleStringProperty(u_lname);
        this.createdAt = new SimpleStringProperty(u_createdAt);
    }


    
    public int getId(){
        return id.get();
    }
    
    public String gefName(){
        return fname.get();
    }
    
    public String getlName(){
        return lname.get();
    }
    
    public String getcreatedAt(){
        return createdAt.get();
    }
    
    public void setId(int value){
        id.set(value);
    }
    
    public void setfName(String value){
        fname.set(value);
    }
    
    public void setlname(String value){
        lname.set(value);
    }
    
    public void setcreatedAt(String value){
        createdAt.set(value);
    }
    
    
    //Property Values
    public IntegerProperty idProperty(){
        return id; 
    }
    
    public StringProperty fnameProperty(){
        return fname;
    }
    
    public StringProperty lnameProperty(){
        return lname;
    }
    
    public StringProperty createdAtProperty(){
        return createdAt;
    }
    
}
